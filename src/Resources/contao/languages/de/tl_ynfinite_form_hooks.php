<?php

$GLOBALS['TL_LANG']['tl_ynfinite_form_hooks']['new'] = array("Neuen Hook", "Neuen Hook bestücken");
$GLOBALS['TL_LANG']['tl_ynfinite_form_hooks']['edit'] = array("Hook bearbeiten", "Hook bearbeiten");
$GLOBALS['TL_LANG']['tl_ynfinite_form_hooks']['delete'] = array("Hook löschen", "Hook löschen");
$GLOBALS['TL_LANG']['tl_ynfinite_form_hooks']['show'] = array("Hook anzeigen", "Hook anzeigen");

$GLOBALS['TL_LANG']['tl_ynfinite_form_hooks']['type'] = array("Typ", "Wählen Sie hier einen Hook ihrer Wahl aus. Eine genaue Beschreibung aller Hooks finden Sie in der Dokumentation.");
$GLOBALS['TL_LANG']['tl_ynfinite_form_hooks']['code'] = array("Code", "Geben Sie hier den JavaScript Code ein der ausgeführt werden soll. Der Code wird vom Plugin in eine Funktion konvertiert.");